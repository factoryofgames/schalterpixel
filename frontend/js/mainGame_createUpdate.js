let fontStyle = { fontFamily: 'MODES', fontSize: 21, color: '#ffeefa' };

let map = {};
let marker = {};
function gameCreate() {
	let headsBoxPos = {x:760,y:10,w:190,h:100,fontSize: 41};

	let boxFontStyle = { fontFamily: 'MODES', fontSize: 28, color: '#ffeefa' };
	let h = 0;
	for(const [key,value] of Object.entries(state.hud.text) ) {
		value.tObj = this.add.text(headsBoxPos.x, headsBoxPos.y  + (h * headsBoxPos.h), value.title,  boxFontStyle);
		value.vObj = this.add.text(headsBoxPos.x, headsBoxPos.y  + (h * headsBoxPos.h) + 50, '    0',  boxFontStyle);
		value.value = 0;
		h++;
	}

	state.level = state.data.allLevel.Levelz[state.data.levelPos];

	state.data.levelPos++;

	const level = genBoard(state.level.width, state.level.height);

	map = this.make.tilemap({data: level, tileWidth: 32, tileHeight: 32});
	let tiles = map.addTilesetImage('schalterBoard');
	let layer = map.createLayer(0,tiles, 0, 0);

	loadLevel(state.level.seed, state.level.extraz);

	marker = this.add.graphics();
  marker.lineStyle(2, 0x0000ae, 1);
  marker.strokeRect(0, 0, map.tileWidth * layer.scaleX, map.tileHeight * layer.scaleY);

  updateAllTextVlaues();
  let that = this;
  goodLevelIcons(that);
  state.hud.won = false;
  state.hud.lastTick = 0;
}

function gameUpdate(time) {
	
	if( !state.hud.won && (state.hud.lastTick + 1000 < time) ) {
		updateValue("time","+1");
		state.hud.lastTick = time;
	}
	let worldPoint = this.input.activePointer.positionToCamera(this.cameras.main);
	let pointerTileX = map.worldToTileX(worldPoint.x);
	let pointerTileY = map.worldToTileY(worldPoint.y);
	if(pointerTileX < map.width && pointerTileY < map.height) {
		marker.x = map.tileToWorldX(pointerTileX);
		marker.y = map.tileToWorldY(pointerTileY);
	}

	if(state.input.lastClick < (time - state.input.clickDelay)  && this.input.manager.activePointer.isDown) {
		state.input.lastClick = time;
		// console.log(pointerTileX, pointerTileY);
		let clTile = map.getTileAt(pointerTileX, pointerTileY, true, 0);
		if(!(clTile||false)) return;
		let tIx = clTile.index;
		let nIx = -1;
		if(tIx===0) {
			nIx = 1;
		} else if(tIx===1) {
			nIx = 2;
		} else if(tIx===2) {
			nIx = 3;
		} else if(tIx===3) {
			nIx = 0;
		} else if(tIx===4) {
			nIx = 5;
		} else if(tIx===5) {
			nIx = 4;
		}
		// map.putTilesAt(nIx, pointerTileX, pointerTileY, true);
		if(nIx!=-1) {
			map.getTileAt(pointerTileX, pointerTileY, true, 0).index = nIx;
			checkPowerLine();
			updateValue('turn',"+1");
		}
	}
	if(state.hud.won) {
		// this.scene.pause('mainGame');
		// state.hud.levelNumber++;
	} else if(state.hud.lost) {
		console.log("you lost a round");
	} else if(state.hud.gameOver) {
		console.log("you lost the game");
	}
}

const genBoard = (width, height) => {
	let nBoard = [];
	nBoard.push(makeLine(width, 16, 17, 19));

	for(let m=1; m < height -2; m++) {
		nBoard.push(makeLine(width, 22, 30, 21));
	}

	nBoard.push(makeLine(width, 24, 26, 27));
	return nBoard;
};

const makeLine = (width, left, mid, right) => {
	let nLine = [];
	for(let w=0; w < width; w++) {
		if(w===0) { 
			nLine[w] = left;
		} else if(w===width-1) {
			nLine[w] = right;
		} else {
			nLine[w] = mid;
		}
	}
	return nLine;
};

const seedLevel = (seed) => {
	let lrnd = new Math.seedrandom(seed);
	for(let h=1,hl=map.height-1; h < hl; h++) {
		for(let w=1,wl=map.width-1; w < wl; w++) {
			let myR = Math.abs(lrnd.int32());
			map.putTilesAt( [myR%6], w, h, true, 0 );
		}
	}
};

const placeExtraTiles = (placed) => {
	state.hud.lightsLeft = 0;
	state.hud.powerList = [];
	state.hud.lampList = [];
	for(let p=0,pl=placed.length;p < pl; p++) {
		// map.putTilesAt( placed[p][0], placed[p][0], placed[p][1] );
		let rTile = map.getTileAt(placed[p][0], placed[p][1], true, 0);
		if(!rTile||false) continue;
		rTile.index = placed[p][2];
		// grab all power sources for the calculation
		if([7, 18, 20, 23, 25].indexOf(placed[p][2])!=-1) {
			state.hud.powerList.push(placed[p]);
		}
		if([8, 10, 12, 14].indexOf(placed[p][2])!=-1) {
			state.hud.lampList.push(placed[p]);
			state.hud.lightsLeft++;
		}
	}
};

const loadLevel = (seed,placed) => {
	seedLevel(seed);
	placeExtraTiles(placed);

	updateValue('lights',state.hud.lightsLeft);
	// console.dir(state.hud.powerList);
};

const checkPowerLine = () => {
	if(state.input.checkRunning) return;
	const startTime = new Date()*1;
	state.input.checkRunning = true;
	// console.log("enter check power");
	let powS = [];
	for(let p=0,pl=state.hud.powerList.length; p < pl; p++) {
		let sLP = state.hud.powerList[p];
		if(sLP[2]===7) {
			powS.push([0,sLP[0],sLP[1]]);
			powS.push([1,sLP[0],sLP[1]]);
			powS.push([2,sLP[0],sLP[1]]);
			powS.push([3,sLP[0],sLP[1]]);
		} else if(sLP[2]===18) {
			powS.push([2,sLP[0],sLP[1]]);
		} else if(sLP[2]===20) {
			powS.push([1,sLP[0],sLP[1]]);
		} else if(sLP[2]===23) {
			powS.push([3,sLP[0],sLP[1]]);
		} else if(sLP[2]===25) {
			powS.push([0,sLP[0],sLP[1]]);
		}
	}
	for(let l=0,ll=state.hud.lampList.length; l < ll; l++) {
		let tileIndex = map.getTileAt(state.hud.lampList[l][0],state.hud.lampList[l][1], true, 0).index;
		if(tileIndex%2!=0) {
			tileIndex--;
		}
		map.getTileAt(state.hud.lampList[l][0],state.hud.lampList[l][1], true, 0).index = tileIndex;
	}
	let lampsOn = state.hud.lightsLeft;
	let loopFuse = 0;
	while(powS.length!=0) {
		loopFuse++;
		if(loopFuse>=100) {
			console.log("Controle Fuse Fired");
			console.dir(powS);
			break;
			powS = [];
		}
		let nextRun = powS.shift();
		let goodOne = lineThread(nextRun[0], nextRun[1], nextRun[2]);
		if(goodOne) {
			if(goodOne.lightOn) {
				// console.log("find lightOn at",goodOne.posx,goodOne.posy);
				if(goodOne.tileIdx%2===0) {
					map.getTileAt(goodOne.posx,goodOne.posy, true, 0).index = ++goodOne.tileIdx;
					lampsOn--;
				}
			} else if(goodOne.allDirs.length>0) {
				for(let g=0,gl=goodOne.allDirs.length; g < gl; g++) {
					powS.push([goodOne.allDirs[g], goodOne.posx, goodOne.posy]);
				}
			}
		}
	}
	updateValue('lights',lampsOn);
	if(lampsOn===0) {
		state.hud.won = true;
	}
	const endTime = new Date()*1;
	// console.log("time to check "+(endTime-startTime));
	// console.log("leave check power");
	state.input.checkRunning = false;
};

const lineThread = (direction, posx, posy) => {
	// dir table
	// 			0
	//			|
	// 3 < - - > 1
	// 			|
	//      2
	if(direction===0) {
		posy -= 1;
	} else if(direction===1) {
		posx += 1
	} else if(direction===2) {
		posy += 1;
	} else if(direction===3) {
		posx -= 1; 
	}
	if(posy>=map.height || posx>= map.width || posx < 0 || posy < 0) {
		return false;
	}

	let nDir = -1;
	let tileIdx = map.getTileAt(posx, posy, true, 0).index;
	if(tileIdx===0 && (direction===2 || direction===3) ) {
		nDir = (direction===2)? 1 : 0;
	} else if(tileIdx===1 && (direction===3 || direction===0) ) {
		nDir = (direction===3)? 2 : 1; 
	} else if(tileIdx===2 && (direction===1 || direction===0) ) {
		nDir = (direction===1)? 2 : 3; 
	} else if(tileIdx===3 && (direction===2 || direction===1) ) {
		nDir = (direction===2)? 3 : 0; 
	} else if(tileIdx===4 && (direction===3 || direction===1) ) {
		nDir = direction;
	} else if(tileIdx===5 && (direction===2 || direction===0) ) {
		nDir = direction;
	} else if(tileIdx===6) {
		let allDirs = [];
		if(direction===0) {
			allDirs = [0,1,3];
		} else if(direction===1) {
			allDirs = [0,1,2];
		} else if(direction===2) {
			allDirs = [1,2,3];
		} else if(direction===3) {
			allDirs = [0,2,3];
		}
		return {posx,posy,allDirs};
	} else if(tileIdx===7) {
		return false;
	} else if( 
							((tileIdx=== 8 || tileIdx=== 9) && direction===2) || 
							((tileIdx===10 || tileIdx===11) && direction===3) ||
							((tileIdx===12 || tileIdx===13) && direction===0) ||
							((tileIdx===14 || tileIdx===15) && direction===1)
						) {
		return {posx,posy,lightOn:true,tileIdx};
	} else {
		return false;
	}
	return lineThread(nDir, posx, posy);
};

const updateValue = (name, nextValue) => {
	let tmpValue = 0;
	let newValue = ""+nextValue;
	if(!state.hud.text[name]||false) return;
	if(newValue.indexOf("-")>=0) {
		tmpValue = state.hud.text[name].value - parseInt(newValue);
	} else if(newValue.indexOf("+")>=0) {
		tmpValue = state.hud.text[name].value + parseInt(newValue);
	} else {
		tmpValue = parseInt(newValue);
	}
	state.hud.text[name].value = tmpValue;
	updateTextValues(state.hud.text[name]);
};

const updateTextValues = (txObj) => {
	let writeStr = "";
	if(txObj.type==="center") {
		writeStr = (txObj.value>100)? '   ' : '    ';
		writeStr += txObj.value;
	} else if(txObj.type==="score") {
		let overLength = "0000000"+txObj.value;
		writeStr = " "+overLength.slice(-7);
	} else if(txObj.type==="time") {
		let timeStr = '';
		const sec = parseInt(txObj.value,10);
		let hours   = Math.floor(sec/3600);
		let minutes = Math.floor( (sec - (hours * 3600)) / 60 );
		let seconds = Math.floor( sec - (hours * 3600) - (minutes * 60) );
		if(hours>0) {
			timeStr += (hours>=10)? hours : "0"+hours;
			timeStr += ":";
		}
		if(minutes>0) { 
			timeStr += (minutes>=10)? minutes : "0"+minutes;
		} else {
			timeStr += "00";
		}
		timeStr += ":"
		timeStr += (seconds>=10)? seconds : "0"+seconds;
		writeStr = (hours>0)? timeStr : "  "+timeStr;
	}
	txObj.vObj.setText(writeStr);
};

const updateAllTextVlaues = () => {
	for(const [key,value] of Object.entries(state.hud.text) ) {
		updateTextValues(value);
	}
};
