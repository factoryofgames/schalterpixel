const editorIcons = (that) => {
	let xLeft = 755;
	let xRight = 905;
	let ix = 0;
	let iy = 0;
	let stoneList = [6,8,10,12,14,34,18,20,23,25,7,35,33,32,31,32,30,29];
	
	state.edit.previewTile = that.add.sprite(xRight+42,423, 'stones', 33);
	state.edit.selectTile = 33;
	state.edit.statusLEDs.upper = that.add.sprite(xRight+42,423+32, 'stones',41);
	state.edit.statusLEDs.lower = that.add.sprite(xRight+42,423+64, 'stones',47);
	
	for(let i=0,il=stoneList.length; i < il; i++) {
		let cross = that.add.sprite(xLeft + (ix*32), 423 + (iy*32), 'stones', stoneList[i]).setInteractive();
		let cplus = stoneList[i];
		// let that = this;
		cross.on('pointerdown', (pointer) => {
			// console.log("click on "+cplus);
			clickEditBtn(that,cplus);
		});
		ix++;
		if(ix>=6) {
			iy++;
			ix=0;
		}
	}

	let seedMinus = that.add.sprite(xLeft, 125, 'stones', 4).setInteractive();
	seedMinus.on('pointerdown', (pointer) => {
		changeSeed('minus');
	});
	let seedPlus = that.add.sprite(xRight, 125, 'stones', 6).setInteractive();
	seedPlus.on('pointerdown', (pointer) => {
		changeSeed('plus');
	});
	let seedRand = that.add.sprite(xRight+42, 125, 'stones', 7).setInteractive();
	seedRand.on('pointerdown', (pointer) => {
		changeSeed('random');
	});

	let widthMinus = that.add.sprite(xLeft, 125+100, 'stones', 4).setInteractive();
	widthMinus.on('pointerdown', (pointer) => {
		changeSize('width','minus');
	});
	let widthPlus = that.add.sprite(xRight, 125+100, 'stones', 6).setInteractive();
	widthPlus.on('pointerdown', (pointer) => {
		changeSize('width','plus');
	});

	let heightMinus = that.add.sprite(xLeft, 125+200, 'stones', 4).setInteractive();
	heightMinus.on('pointerdown', (pointer) => {
		changeSize('height','minus');
	});
	let heightPlus = that.add.sprite(xRight, 125+200, 'stones', 6).setInteractive();
	heightPlus.on('pointerdown', (pointer) => {
		changeSize('height','plus');
	});
};

const clickEditBtn = (that,tileNo) => {
	// console.log(tileNo);
	state.edit.previewTile.setFrame(tileNo);
	state.edit.selectTile = tileNo;
	if(tileNo===29) {
		//
		let undoObj = {
			levelID:state.edit.text.levelID.value,
			seed:state.edit.text.seed.value,
			width:state.edit.text.width.value,
			height:state.edit.text.height.value,
			extraz:state.edit.extraTiles
		};
		state.edit.undo.push(undoObj);
		state.level.levelID = state.edit.text.levelID.value;
		state.level.seed = state.edit.text.seed.value;
		state.level.width = state.edit.text.width.value;
		state.level.height = state.edit.text.height.value;
		state.level.extraz = [];

		stopStartScene(that, 'editGame', 'editGame');
	
	} else if(tileNo===34) { // undo button
		
		doTheUndo();
	} else if(tileNo===35) { // redo button -- maybe not so soon !
		
	} else if(tileNo===31) {
		saveEditLevel();
	}
};

const goodLevelIcons = (that) => {
	let xLeft = 750;
	let xRight = 905;
	let restart = that.add.sprite(xRight+42,423, 'stones', 33).setInteractive();
	restart.on('pointerdown', (pointer) => {
		state.data.levelPos--;
		that.scene.stop('mainGame');
		that.scene.start('mainGame');
	});
	let next = that.add.sprite(xRight+42,423+32, 'stones',29).setInteractive();
	next.on('pointerdown', (pointer) => {
		that.scene.stop('mainGame');
		that.scene.start('mainGame');
	});

	let save = that.add.sprite(xRight+42,423+64, 'stones',31).setInteractive();
	save.on('pointerdown', (pointer) => {
		console.dir(saveGoodLevel(1));
	});

	let badone = that.add.sprite(xRight+42,423+96, 'stones',30).setInteractive();
	badone.on('pointerdown', (pointer) => {
		console.dir(saveGoodLevel(0));
	});

	state.edit.statusLEDs.lower = that.add.sprite(xRight-42, 423+96, 'stones', 44);
};

const addFullScreenIcon = (that) => {
	let fIcon = that.add.sprite(config.width-20, 20, 'stones', 36).setInteractive(); // 36=toFull 37=fromFull
	fIcon.setDisplaySize(16,16);
	fIcon.on('pointerup', () => {
		if(that.scale.isFullscreen) {
			fIcon.setFrame(36);
			that.scale.stopFullscreen();
		} else {
			fIcon.setFrame(37);
			that.scale.startFullscreen();
		}
	});
};