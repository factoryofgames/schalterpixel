function editCreate() {
	let headsBoxPos = {x:760,y:10,w:190,h:100,fontSize: 41};
	let boxFontStyle = { fontFamily: 'MODES', fontSize: 28, color: '#ffeefa' };

	let h = 0;
	for(const [key,value] of Object.entries(state.edit.text) ) {
		value.value = state.level[key]||0;
		value.tObj = this.add.text(headsBoxPos.x, headsBoxPos.y  + (h * headsBoxPos.h), value.title,  boxFontStyle);
		value.vObj = this.add.text(headsBoxPos.x + value.xPush, headsBoxPos.y  + (h * headsBoxPos.h) + 50, value.value,  boxFontStyle);
		h++;
	}
	updateLevelID();

	const level = genBoard(state.level.width, state.level.height);

	map = this.make.tilemap({data: level, tileWidth: 32, tileHeight: 32});
	let tiles = map.addTilesetImage('schalterBoard');
	let layer = map.createLayer(0,tiles, 0, 0);

	seedLevel(state.level.seed);

	// marker = this.add.graphics();
  // marker.lineStyle(2, 0x0000ae, 1);
  // marker.strokeRect(0, 0, map.tileWidth * layer.scaleX, map.tileHeight * layer.scaleY);

  state.edit.cursorSprite = this.add.sprite(-64, -64, 'stones', 32);
  state.edit.cursorSprite.setDisplayOrigin(0.5,0.5);

  editorIcons(this);
  addFullScreenIcon(this);
}

function editUpdate(time) {
	let worldPoint = this.input.activePointer.positionToCamera(this.cameras.main);
	let pointerTileX = map.worldToTileX(worldPoint.x);
	let pointerTileY = map.worldToTileY(worldPoint.y);
	let powerSide = false;

	if(pointerTileX < map.width && pointerTileY < map.height) {
		let tilePosX = pointerTileX*32;
		let tilePosY = pointerTileY*32;
		let cursorFame = 33;

		if ((pointerTileX===0&&pointerTileY===0) || (pointerTileX===0&&pointerTileY===map.height-1) || (pointerTileY===0&&pointerTileX===map.width-1) || (pointerTileX===map.width-1&&pointerTileY===map.height-1)) {
			cursorFame = 38;
		} else if(pointerTileX===0) {
			cursorFame = 20;
		} else if(pointerTileX===map.width-1) {
			cursorFame = 23;
		} else if(pointerTileY===0) {
			cursorFame = 18;
		} else if(pointerTileY===map.height-1) {
			cursorFame = 25;
		} else {
			cursorFame = state.edit.selectTile;
		}
		state.edit.cursorSprite.setFrame(cursorFame);

		state.edit.cursorSprite.x = tilePosX;
		state.edit.cursorSprite.y = tilePosY;

		// marker.x = map.tileToWorldX(pointerTileX);
		// marker.y = map.tileToWorldY(pointerTileY);
	}

	if(state.input.lastClick < (time - state.input.clickDelay)  && this.input.manager.activePointer.isDown) {
		state.input.lastClick = time;
		// console.log(pointerTileX, pointerTileY);
		let clTile = map.getTileAt(pointerTileX, pointerTileY, true, 0);
		// let reTile = state.edit.previewTile;
		if(!(clTile||false)) return;		
		if(state.edit.selectTile===33) {
			let tIx = clTile.index;
			let nIx = -1;
			if(tIx===0) {
				nIx = 1;
			} else if(tIx===1) {
				nIx = 2;
			} else if(tIx===2) {
				nIx = 3;
			} else if(tIx===3) {
				nIx = 0;
			} else if(tIx===4) {
				nIx = 5;
			} else if(tIx===5) {
				nIx = 4;
			}
			if(nIx!=-1) {
				map.getTileAt(pointerTileX, pointerTileY, true, 0).index = nIx;
			}
		} else if([18,20,23,25].indexOf(state.edit.cursorSprite.frame.name)>=0) {
			setTileOntoMap(pointerTileX, pointerTileY, state.edit.cursorSprite.frame.name);
		} else if([6,7,8,10,12,14].indexOf(state.edit.selectTile)>=0) {
			setTileOntoMap(pointerTileX, pointerTileY, state.edit.selectTile);
		}
	// console.log(state.edit.selectTile);
	}
	// LED magic
	if(state.edit.statusLEDs.start && state.edit.statusLEDs.blinkT < (time - 500)) {
		if(state.edit.statusLEDs.seqPos>=state.edit.statusLEDs.seq.length) {
			state.edit.statusLEDs.seqPos = 0;
			state.edit.statusLEDs.start = false;
		}
		// state.edit.statusLEDs.lower.setFrame(state.edit.statusLEDs.upper.frame.name);
		state.edit.statusLEDs.upper.setFrame(state.edit.statusLEDs.seq[state.edit.statusLEDs.seqPos]);
		state.edit.statusLEDs.seqPos++;
		state.edit.statusLEDs.blinkT = time;
	}
}

const findXYinExtraz = (x,y) => {
	let found = false;
	for(let f=0,fl=state.level.extraz.length; f < fl; f++) {
		let entry = state.level.extraz[f];
		if(	entry[0] === x &&
				entry[1] === y) {
			found = f;
			break;
		}
	}
	return found;
};

const addUniqExtraTile = (pointerTileX, pointerTileY, selectedTile) => {
	let found = findXYinExtraz(pointerTileX, pointerTileY);
	if(found===false) {
		state.level.extraz.push([pointerTileX, pointerTileY, selectedTile]);
	} else if(found>=0||false) {
		state.level.extraz[found][2] = selectedTile;
	}
};

const removeUniqExtraTile = (pointerTileX, pointerTileY, oldTileIndex, newTileIndex) => {
	let found = findXYinExtraz(pointerTileX, pointerTileY);
	if(oldTileIndex<=5 && found) {
		state.level.extraz.splice(found,1);
	} else {
		addUniqExtraTile(pointerTileX, pointerTileY, newTileIndex);
	}
	return found;
};

const calcLevelID = () => {
	let w = ""+state.edit.text.width.value;
	let h = "00"+state.edit.text.height.value;
	let s = ""+state.edit.text.seed.value;
	let leIDstr = parseInt(w + h.slice(-2) + s);
	let nLevelId = leIDstr.toString(16).toUpperCase();
	// console.log(nLevelId);
	return nLevelId;
};

const deConstructLevelID = (levelID) => {
	let lNo = parseInt(levelID,16)+"";
	let w = parseInt(lNo.substr(0,2));
	let h = parseInt(lNo.substr(2,2));
	let seed = parseInt(lNo.substr(4));
	// console.log(w, h, seed);
	return [w, h, seed];
};

const changeSeed = (what) => {
	if(what==='minus') {
		state.edit.text.seed.value--;
	} else if(what==='plus') {
		state.edit.text.seed.value++;
	} else if(what==='random') {
		state.edit.text.seed.value = Math.floor(Math.random()*1000000);
	}
	state.edit.text.seed.vObj.setText(state.edit.text.seed.value);
	updateLevelID();
};

const changeSize = (which,what) => {
	if(which==='width') {
		if(what==='minus') {
			state.edit.text.width.value--;			
		} else if(what==='plus') {
			state.edit.text.width.value++;
		}
		if(state.edit.text.width.value<10) {
			state.edit.text.width.value = 10;
		} else if(state.edit.text.width.value>23) {
			state.edit.text.width.value = 23;
		}
		state.edit.text.width.vObj.setText(state.edit.text.width.value);
	} else if(which==='height') {
		if(what==='minus') {
			state.edit.text.height.value--;			
		} else if(what==='plus') {
			state.edit.text.height.value++;
		}
		if(state.edit.text.height.value<8) {
			state.edit.text.height.value = 8;
		} else if(state.edit.text.height.value>18) {
			state.edit.text.height.value = 18;
		}
		state.edit.text.height.vObj.setText(state.edit.text.height.value);		
	}
	updateLevelID();
};

const updateLevelID = () => {
	const newLevelID = calcLevelID();
	state.edit.text.levelID.value = newLevelID;
	state.edit.text.levelID.vObj.setText(newLevelID);
};

const drawExtraz = () => {
	for(let e=0,el=state.level.extraz.length; e < el; e++) {
		map.getTileAt(state.level.extraz[e][0],state.level.extraz[e][1]).index = state.level.extraz[e][2];
	}
};

const setTileOntoMap = (tileX, tileY, tileIndex) => {
	let lastTile = map.getTileAt(tileX, tileY).index;
	state.edit.undoTiles.push([tileX,tileY,lastTile]);

	map.getTileAt(tileX, tileY).index = tileIndex;
	addUniqExtraTile(tileX, tileY, tileIndex);
};

const doTheUndo = () => {
	if(state.edit.undoTiles.length===0) return;
	let lastTile = state.edit.undoTiles.pop();
	state.edit.redoTiles.push(lastTile);
	let nowTileIndex = map.getTileAt(lastTile[0],lastTile[1]).index;
	let found = removeUniqExtraTile(lastTile[0], lastTile[1], nowTileIndex, lastTile[2]);
	map.getTileAt(lastTile[0],lastTile[1]).index = lastTile[2];
};