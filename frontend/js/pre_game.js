function init() {
  //  Inject our CSS
  let element = document.createElement('style');
  document.head.appendChild(element);
  let sheet = element.sheet;
  let styles = '@font-face { font-family: "MODES"; src: url("assets/fonts/MODES.otf") format("opentype"); }\n';
  sheet.insertRule(styles, 0);
  // let lastHighScore = localStorage.getItem('highscore');
  // if(lastHighScore||false) {
  //   hud.values.highscore = parseInt(lastHighScore);
  // } else {
  //   localStorage.setItem('highscore',1000);
  // } 
}

function gamePreload() {

  let progress = this.add.graphics();
  let countFont = this.add.text(10,10,'',fontStyle);
  this.load.on('fileprogress', function (file, value) {
    if (file.key === 'gameLogo'){
      progress.clear();
      progress.fillStyle(0xffffff, 0.4);
      progress.fillRect(450, 500 - (value * 400), 200, value * 400);
      countFont.setText(value);
    }
  });

  this.load.on('complete', function () {
    progress.destroy();
    countFont.destroy();
  });

  this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js');
  this.load.image('gameLogo', '/assets/images/schalterPicture.png');
  this.load.image('schalterBoard', '/assets/tiles/schalterBoard.png');
  this.load.spritesheet('stones', '/assets/tiles/schalterBoard.png', { frameWidth: 32, frameHeight: 32 });

  this.load.json('allLevelJson', '/get/unplayed/10');
}

let gameLogo;

function gamePreCreate() {
  gameLogo = this.add.image(config.width/2, config.height/2, 'gameLogo');
  gameLogo.setAlpha(0);

// pointerdown event fire over to next scene
// avoid that by poinerup

  this.input.on('pointerup', function (pointer) {
    stopStartScene(this, 'preGame', 'mainMenu');
    }, this);

  let spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
  spaceKey.on('down', (key, event) => {
    stopStartScene(this, 'preGame', 'mainMenu');
  });

  // just an early random seed
  state.level.seed = Math.floor(Math.random()*10000000);
  state.data.allLevel = this.cache.json.get('allLevelJson');
  state.data.levelPos = 0;
}

function gamePreUpdate() {
  let toGo = gameLogo.alpha;
  if(toGo>=1) {
    stopStartScene(this, 'preGame', 'mainMenu');
  } else {
    gameLogo.setAlpha(toGo+.005);
  }
}

const stopStartScene = (that, stopName, startName) => {
  that.scene.stop(stopName);
  setTimeout( () => { that.scene.start(startName);}, 150);
};