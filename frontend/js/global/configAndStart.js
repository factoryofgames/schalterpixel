const config = {
  type: Phaser.AUTO,
  width: 968,
  height: 544,
  pixelArt: true,
  backgroundColor: 0x000000,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: 968,
    height: 544
  },
  scene: [
    { key: 'preGame', active: true, init: init,  preload: gamePreload, create: gamePreCreate, update: gamePreUpdate },
    { key: 'mainMenu', active: false, create: menuCreate, update: menuUpdate },
    { key: 'mainGame', active: false, create: gameCreate, update: gameUpdate },
    { key: 'editGame', active: false, create: editCreate, update: editUpdate },
    ],
};

let game = new Phaser.Game(config);

let state = {};
state.input = {
  clickDelay: 300,
  checkRunning: false,
  lastClick: 0,
  lastTile: 0,
};

state.level = {
  seed: 43,
  width: 10,
  height: 8,
  extraz: [ [20, 0, 1], [23, 9, 5], [8, 5, 2], [18, 6, 0], [25, 1, 6], [10, 4, 4], [14, 8, 2], [12, 2, 4]],
};

state.data = {
  allLevel: {},
  levelPos: 8
};

state.hud = {
  text: {
    lights: {title:" Lights",type:"center",tObj:{},value:0,vObj:{}},
    time: {title:"  Time",type:"time",tObj:{},value:0,vObj:{}},
    turn: {title:"  Turns",type:"center",tObj:{},value:0,vObj:{}},
    score: {title:"  Score",type:"score",tObj:{},value:0,vObj:{}},
    highscore: {title:"Highscore",type:"score",tObj:{},value:0,vObj:{}},
  },
  powerList: [],
  lampList: [],
  lightsLeft: 99,
  timeLeft: 100,
  lastTick: 0,
  won: false,
  lost: false,
  gameOver: false,
};

state.menu = {
  pos: 0,
  lastY: 0,
  keys: {},
  spaceing: 0,
  prell: 0,
  select: false,
  main: {
    start:{title:"Start Game",xPush:0,tObj:{},action:"startGame"},
    editor:{title:"Editor",xPush:42,tObj:{},action:"startEditor"},
    options:{title:"Options",xPush:36,tObj:{},action:"subOptions"},
    credits:{title:"Credits",xPush:36,tObj:{},action:"startCreadits"},
  },
  subOptions: {
    startAt:{title:"Level Start",tObj:{},action:"addLevelStart",value:0},
    difficulty:{title:"Difficulty",tObj:{},action:"addDifficulty",value:0},
    backMainMenu:{title:"Back", tObj:{}, action:"backMainMenu",value:0}
  }
};

state.edit = {
  mode: true,
  selectTile: 0,
  previewTile: {},
  statusLEDs: {upper:{},lower:{},blinkT:0,seq:[40,41,43,40,41],seqPos:0,start:false},
  cursorSprite: {},
  extraz: [],
  levelSaved: false,
  text: {
    levelID:{title:"Level ID",xPush:0,tObj:{},value:"x0x0x0",vObj:{}},
    seed:{title:"  Seed",xPush:20,tObj:{},value:"15486945325",vObj:{}},
    width:{title:"  Width",xPush:50,tObj:{},value:12,vObj:{}},
    height:{title:" Height",xPush:60,tObj:{},value:8,vObj:{}},
  },
  undo: [],
  undoTiles: [],
  redoTiles: [],
};