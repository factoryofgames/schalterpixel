const express = require('express');
const fs = require('fs');

const rdb = require('./databaseLib.js');

const host = "0.0.0.0";
const port = 8042;

const app = express();
app.use(express.static('frontend'));
app.use(express.json());

app.listen(port, host);
console.log(`Running on http://${host}:${port}`);

app.post('/savelevel', (req, res) => { 
	console.dir(req.body);
	let rawdata = fs.readFileSync('levelDB.js');
	let allLevel = JSON.parse(rawdata);
	allLevel.Levelz.push(req.body.data);
	let writeData = JSON.stringify(allLevel);
	fs.writeFileSync('levelDB.js',writeData);
	res.json(req.body.data);
});

app.get('/loadlevel', (req, res) => {
	let rawdata = fs.readFileSync('levelDB.js');
	let allLevel = JSON.parse(rawdata);
	res.json(allLevel);
});

app.post('/save/good/level', async (req, res) => {
	let bodyJ = req.body.data;
	console.dir(req.body);
	let reStatus = {good:false};
	if(bodyJ||false) {
		await loadSaveUpdateJSON('goodLevel.js', 'good', bodyJ);
		reStatus.good = true;
	}
	res.json(reStatus);
});

const saveJSON = async (filename, data) => {
	let writeData = JSON.stringify(data);
	fs.writeFileSync(filename, writeData);
	return true;
};

const loadJSON = async (filename) => {
	let rawdata = fs.readFileSync(filename);
	let toJSON = JSON.parse(rawdata);
	return toJSON;
};

const loadSaveUpdateJSON = async (filename, addKey ,addData) => {
	let getJ = await loadJSON(filename);
	getJ[addKey].push(addData);
	let putJ = await saveJSON(filename, getJ);
	return true;
};

const checkInputBoard = async(jObj) => {
	let failed = false;
	if(!(jObj.levelID||false)) failed = true;
	if(!(jObj.seed||false)) failed = true;
	if(!(jObj.width||false)) failed = true;
	if(!(jObj.height||false)) failed = true;
	if(!(jObj.extraz||false)) failed = true;
	return failed;
};

app.get('/get/level/:levelID', async (req,res) => {
  let reList = {Levelz:[]};
  let levelID = req.params.levelID;
  if(levelID!="") {
    reList.Levelz = await rdb.getLevelByID(levelID);
  }
  res.status(200).json(reList);
});

app.post('/post/level', async (req,res) => {
	console.dir(req.body.data);
	// let levelData = JSON.parse(req.body.data);
	let levelData = req.body.data;
	// {"data":{"levelID":"5A5585DB","seed":554267,"width":15,"height":15,"extraz":[[3,12,8],[1,13,25],[13,9,8],[5,13,25],[11,10,14],[14,12,23],[2,6,14],[0,3,20],[10,6,10],[14,6,23],[10,4,10],[11,0,18],[1,1,10],[0,2,20],[6,2,10],[6,0,18],[6,8,8],[8,0,18]]}}

	let reInsert = {};
	let statusCode = 207;
	if(await checkInputBoard(levelData)) { 
		reInsert =  {err:"you input is wrong"};
	} else {
		try {
			reInsert = await rdb.insertIntoBoards(levelData.levelID, levelData.width, levelData.height, levelData.seed, levelData.extraz);
			// console.dir(reInsert);
			if(reInsert.err||false) {
				statusCode = 206;
			} else {
				statusCode = 202
			}
		} catch {
			reInsert =  {err:"sql error while request"};
			statusCode = 201;
		}
	}
	res.status(statusCode).json(reInsert);
});


app.post('/post/played', async (req,res) => {
	let levelData = req.body.data;
	let reObj = {err:null, status: "empty"};
	let statusCode = 207;
	if(await checkInputBoard(levelData)) {
		reObj.err = "your input is wrong";
		reObj.status = "check Input Board failed true";
		statusCode = 204;
	} else {
		reInto = await rdb.insertIntoPlayed(levelData.levelID, levelData.extraz, levelData.turns, levelData.time, levelData.won);
		reObj.reval = reInto.reval;
		if(reInto.err||false) {
			statusCode = 206;
		} else {
			statusCode = 202;
			reObj.status = "inserted";
		}
	}
	res.status(statusCode).json(reObj);
});


app.get('/get/unplayed/:nthLevel', async (req,res) => {
	let nthLevel = (req.params.nthLevel||false)? req.params.nthLevel : 10;
	let reLevelz = await rdb.getUnplayeDnLevel(nthLevel);
	console.log(reLevelz);
	res.status(200).json(reLevelz);
});

module.exports = app;