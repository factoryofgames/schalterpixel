const mysql = require('mysql');
const tkt = require('./database.config.js');

const rdb = {};
const db_config = {};

db_config.host = tkt.rdb.host;
db_config.user = tkt.rdb.user;
db_config.password = tkt.rdb.password;
db_config.database = tkt.rdb.database;
db_config.connectionLimit = 10;

rdb.pool = mysql.createPool(db_config);

rdb.poolQ = (sqlQ,args) => {
  return new Promise( ( resolve, reject ) => {
    rdb.pool.getConnection( (err, conn) => {
      if(err) {
        return reject(err);
      } else if(conn) {
        conn.query(sqlQ, args, (err, rows) => {
          conn.destroy();
          if(err) {
            return reject(err);
          } else if(rows){
            return resolve(rows);
          } else {
            return reject({err,rows});
          }
        });
      }
    });
  });
};

rdb.esc = (evilstring) => {
	return rdb.pool.escape(evilstring);
};

rdb.getLevelByID = async (levelID) => {
  let dbQuery = `SELECT * FROM boards WHERE LevelID=${rdb.esc(levelID)};`;
  let oneLevel = await rdb.poolQ(dbQuery);
  return oneLevel;
};

rdb.insertIntoBoards = async (levelID, levelWidth, levelHeight, levelSeed, levelExtraz) => {
  // console.log("i am into sql");
  let doubCheck = `SELECT * FROM boards WHERE LevelID=${rdb.esc(levelID)} AND LevelWidth=${rdb.esc(levelWidth)} AND LevelHeight=${rdb.esc(levelHeight)} AND LevelSeed=${rdb.esc(levelSeed)} AND LevelExtraz=${rdb.esc(JSON.stringify(levelExtraz))};`;
  // console.log("doubCheck "+doubCheck);
  let reCheckEx = await rdb.poolQ(doubCheck);
  if(reCheckEx.length>=1) {
    return {err:`board allready saved`};
  }

  let intoBoards = `INSERT INTO boards (LevelID, LevelWidth, LevelHeight, LevelSeed, LevelExtraz)`;
  intoBoards += `VALUES( ${rdb.esc(levelID)}, ${rdb.esc(levelWidth)}, ${rdb.esc(levelHeight)}, ${rdb.esc(levelSeed)}, ${rdb.esc(JSON.stringify(levelExtraz))});`;
  // console.log("intoBoards "+intoBoards);

  let reInto = await rdb.poolQ(intoBoards);
  if(reInto.length===0) return {err:`error while insert to tasklist SQL: ${intoList}`};
  return {err:null, reval: reInto};
};

rdb.insertIntoPlayed = async (levelID, levelExtraz, turns, time, won) => {
  let getDBid = `SELECT id FROM boards WHERE LevelID=${rdb.esc(levelID)} AND LevelExtraz=${rdb.esc(JSON.stringify(levelExtraz))};`;
  console.log(getDBid);
  let dbID = await rdb.poolQ(getDBid);
  if(dbID.length===0) {
    return {err:`levelID not found`};
  } else {
    let playedQuery = `INSERT INTO playedLevel (board_ID, Turns, Time, Won) VALUES (${rdb.esc(dbID[0].id)},${rdb.esc(turns)},${rdb.esc(time)},${rdb.esc(won)});`;
    let reInsert = await rdb.poolQ(playedQuery);
    console.dir(reInsert);
    return {err:null, reval: reInsert};  
  }
};

rdb.getUnplayeDnLevel = async (nextNthLevel) => {
  let setLimit = (parseInt(nextNthLevel)||false)? parseInt(nextNthLevel) : 10;
  let nextQuery = `SELECT * FROM boards WHERE id NOT IN (SELECT board_ID FROM playedLevel) LIMIT ${nextNthLevel};`;
  console.log(nextQuery);
  let nextL = await rdb.poolQ(nextQuery);
  if(nextL.length===0) {
    return {err: `empty bucket`};
  } else {
    let Levelz = [];
    for(let i=0,il=nextL.length; i < il; i++) {
      let entry = {};
      entry.levelID = nextL[i].LevelID;
      entry.width = nextL[i].LevelWidth;
      entry.height = nextL[i].LevelHeight;
      entry.seed = nextL[i].LevelSeed;
      entry.extraz = JSON.parse(nextL[i].LevelExtraz);
      Levelz.push(entry);
    }
    return {err: null, Levelz};
  }
};



module.exports = rdb;