function menuCreate() {
	let boxFontStyle = { fontFamily: 'MODES', fontSize: 42, color: '#ffeefa' };
	let spaceing = config.height/5;
	state.menu.spaceing = spaceing;

	let step = 1;
	for(const [key,value] of Object.entries(state.menu.main)) {
		value.tObj = this.add.text( (config.width/3) + value.xPush, step * spaceing, value.title, boxFontStyle);
		step++;
	}		
	// this.add.text(config.width/3, spaceing, 'Start Game',  boxFontStyle);
	// this.add.text(config.width/3, 2*spaceing, ' Options',  boxFontStyle);
	// this.add.text(config.width/3, 3*spaceing, ' Credits',  boxFontStyle);


	marker = this.add.graphics();
  marker.lineStyle(2, 0xff0000, 1);
  marker.strokeRect(250, spaceing, 400, 50);

  state.menu.keys = this.input.keyboard.addKeys('W,A,S,D,UP,DOWN,RIGHT,LEFT,ENTER');
}

function menuUpdate(time) {
	let changePos = '';
	let worldPoint = this.input.activePointer.positionToCamera(this.cameras.main);
	if(Math.abs(state.menu.lastY - worldPoint.y)>75) {
		let toPos = Math.floor(worldPoint.y / state.menu.spaceing)-1;
		state.menu.pos = toPos;
		changePos = "update";
		state.menu.lastY = worldPoint.y;
	}

	if(state.menu.prell+300 < time) {	
		if(state.menu.keys.W.isDown || state.menu.keys.UP.isDown) {
			changePos = "up";
		} else if(state.menu.keys.S.isDown || state.menu.keys.DOWN.isDown) {
			changePos = "down";
		}
	}

	if(changePos!="") {
		// console.log("change pos "+changePos);
		moveCursor(changePos);
		state.menu.prell = time;
	}

	if(this.input.manager.activePointer.isDown || state.menu.keys.ENTER.isDown) {
		let menuEntry = Object.entries(state.menu.main)[state.menu.pos];
		// console.log(time);
		// console.dir(menuEntry);
		menuAction(this,menuEntry);
	}

}

const moveCursor = (direction) => {
	if(direction==="down") {
		++state.menu.pos;
	} else if(direction==="up") {
		--state.menu.pos;
	}
	
	if(state.menu.pos>=4) {
		state.menu.pos = 0;
	} else if(state.menu.pos<0) {
		state.menu.pos = 3;
	}
	marker.y = state.menu.spaceing * state.menu.pos;
};

const menuAction = (that, menuEntry) => {
	if(state.menu.select) return;
	state.menu.select = true;
	let entryName = menuEntry[0];
	if(entryName==="editor") {
		stopStartScene(that, 'mainMenu', 'editGame');
	} else if(entryName==="start") {
		stopStartScene(that, 'mainMenu', 'mainGame');
	} else if(entryName==="credits") {
		state.menu.select = false;
	} else if(entryName==="options") {
		state.menu.select = false;
	}
	// console.dir(menuEntry);
};