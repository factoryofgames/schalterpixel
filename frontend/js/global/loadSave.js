// const saveLevel = () => {
// 	state.edit.levelSaved = true;
// 	let sendData = {};
// 	sendData.levelID = state.level.levelID;
// 	sendData.seed = state.level.seed;
// 	sendData.width = state.level.width;
// 	sendData.height = state.level.height;
// 	sendData.extraz = state.level.extraz;

// 	const onprogressing = () => { state.edit.statusLEDs.start = true;};
// 	const onloading = () => { state.edit.statusLEDs.lower.setFrame(41);};
// 	const onerroring = () => { state.edit.statusLEDs.lower.setFrame(44);};

// 	let xhr = new XMLHttpRequest();
// 	xhr.addEventListener("progress", onprogressing);
// 	xhr.addEventListener("load", onloading);
// 	xhr.addEventListener("error", onerroring );

// 	xhr.open("POST", '/post/level', true);
// 	xhr.setRequestHeader('Content-Type', 'application/json');
// 	xhr.send(JSON.stringify({
// 	    data: sendData
// 	}));
// };

const rawSaveToServer = (url, sendData, callProgressing, callLoading, callError) => {
	let xhr = new XMLHttpRequest();
	if(callProgressing||false) {
		xhr.addEventListener("progress", callProgressing);
	}

	if(callLoading||false) {
		xhr.addEventListener("load", callLoading);
	}

	if(callError||false) {
		xhr.addEventListener("error", callError);
	}

	xhr.open("POST", url, true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify({
		data: sendData
	}));
};

const saveEditLevel = () => {
	let sendData = {};
	sendData.levelID = state.level.levelID;
	sendData.seed = state.level.seed;
	sendData.width = state.level.width;
	sendData.height = state.level.height;
	sendData.extraz = state.level.extraz;

	const onprogressing = () => { state.edit.statusLEDs.start = true;};
	const onloading = () => { state.edit.statusLEDs.upper.setFrame(40);};
	const onerroring = () => { state.edit.statusLEDs.upper.setFrame(44);};

	rawSaveToServer('/post/level', sendData, onprogressing, onloading, onerroring);
};

const saveGoodLevel = (didWon) => {
	let sendData = {};
	sendData.levelID = state.level.levelID;
	sendData.seed = state.level.seed;
	sendData.width = state.level.width;
	sendData.height = state.level.height;
	sendData.extraz = state.level.extraz;
	sendData.time = state.hud.text.time.value;
	sendData.turns = state.hud.text.turn.value;
	sendData.won = didWon;

	const onprogressing = () => { state.edit.statusLEDs.lower.setFrame(40);};
	const onloading = () => { state.edit.statusLEDs.lower.setFrame(41);};
	const onerroring = () => { state.edit.statusLEDs.lower.setFrame(44);};

	rawSaveToServer('/post/played', sendData, onprogressing, onloading, onerroring);
	return sendData;
};